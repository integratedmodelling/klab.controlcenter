# Changelog

All notable changes to this project are documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), 
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.4.12] -- 2021/09/15
## Improvements
- added whats new text
- ask confirmation to stop engine and close modeler
- ask if the user want to stop engine and close modeler on exit
### Solved
- switch of develop/master branches doesn't work the first time

## [1.4.03] -- 2021/09/10
### Improvements
- change JRE download (OpenJDK 16)
- change control center download url
- change engine and modeler download url
- confirm close of engine and modeler
- possibility to close engine and modeler on control center close
### Solved
- check the expiry date only from hub
- various issue solved

## [1.3.02] -- 2021/09/03
### Added
- Begin keeping this changelog
- Make repository public

