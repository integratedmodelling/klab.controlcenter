package org.integratedmodelling.controlcenter.product;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.apache.commons.io.FileUtils;
import org.integratedmodelling.controlcenter.ControlCenter;
import org.integratedmodelling.controlcenter.api.IInstance;
import org.integratedmodelling.controlcenter.api.IProduct;
import org.integratedmodelling.controlcenter.product.Distribution.SyncListener;
import org.integratedmodelling.controlcenter.product.Product.Build;
import org.integratedmodelling.klab.Version;

public abstract class Instance implements IInstance {

	protected Product product;
	protected AtomicReference<Status> status = new AtomicReference<>(Status.UNKNOWN);
	protected DefaultExecutor executor;
	protected Consumer<Status> statusHandler;

	public Instance(Product product) {
		this.product = product;
	}

	@Override
	public IProduct getProduct() {
		return product;
	}
	
	public void setProduct(IProduct product) {
		this.product = (Product)product;
	}

	@Override
	public Status getStatus() {
		return status.get();
	}

	protected abstract CommandLine getCommandLine(int build);

	protected File getWorkingDirectory(int build) {
		return product.getBuild(build).workspace;
	}

	protected abstract boolean isRunning();

	@Override
	public boolean start(int build) {

		CommandLine cmdLine = getCommandLine(build);

		/*
		 * assume error was reported
		 */
		if (cmdLine == null) {
			return false;
		}

		this.executor = new DefaultExecutor();
		this.executor.setWorkingDirectory(getWorkingDirectory(build));

		Map<String, String> env = new HashMap<>();
		env.putAll(System.getenv());

		status.set(Status.WAITING);
		if (this.statusHandler != null) {
			this.statusHandler.accept(status.get());
		}

		try {
			this.executor.execute(cmdLine, env, new ExecuteResultHandler() {

				@Override
				public void onProcessFailed(ExecuteException arg0) {
					arg0.printStackTrace();
					status.set(Status.ERROR);
					if (statusHandler != null) {
						statusHandler.accept(status.get());
					}
				}

				@Override
				public void onProcessComplete(int arg0) {
//					status.set(Status.STOPPED);
//					if (statusHandler != null) {
//						statusHandler.accept(status.get());
//					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			status.set(Status.ERROR);
		}

		return true;
	}

	@Override
	public boolean stop() {
		// does nothing - override
		return false;
	}

	@Override
	public List<Integer> getInstalledBuilds() {
		List<Integer> ret = new ArrayList<>();
		File ws = this.product.getLocalWorkspace();
		
		for (File bws : ws.listFiles()) {
			if (bws.isDirectory() && new File(bws + File.separator + "filelist.txt").exists()) {
				ret.add(Integer.parseInt(bws.getName()));
			}
		}
		/*
		 * most recent first
		 */
		ret.sort((o1, o2) -> o2.compareTo(o1));
		
		return ret;
	}
	
	public void cleanOldBuilds() {
	    File ws = this.product.getLocalWorkspace();
	    int toKeep = ControlCenter.INSTANCE.getSettings().resetAllBuildsButLatest() ? 1 : ControlCenter.INSTANCE.getSettings().buildsToKeep();
	    if (ws.listFiles().length > toKeep) {
	        File[] files = ws.listFiles();
	        Arrays.stream(files).sorted((File f1, File f2) -> {
	            int i1 = -1;
	            int i2 = -1;
	            try {
                    i1 = Integer.parseInt(f1.getName(), 10);
                    i2 = Integer.parseInt(f2.getName(), 10);
                    return (i1 > i2 ? 1 : (i1 == i2 ? 0 : -1));
                } catch (NumberFormatException e) {
                    return (i1 == -1 ? -1 : 1);
                }
	        })
	        .limit(files.length - toKeep)
	        .forEach((folder) -> {
	            try {
	                FileUtils.deleteDirectory(folder);
	            } catch (IOException e) {
	                System.err.println("Exception deleting " + folder + ": " + e);
	            }
	        });   
	    }
	}

	@Override
	public Distribution download(int buildNumber, SyncListener listener) {

		Build build = this.product.getBuild(buildNumber);
		if (build != null) {

			int previous = -1;
			for (int n : getInstalledBuilds()) {
				if (n < buildNumber) {
					previous = n;
					break;
				}
			}

			if (previous > 0) {
				/*
				 * preload the worspace with the previous distribution for incremental download.
				 * Skip if the previous version is not valid because doesn't exists in remote and if exists
				 * has not correct min version
				 */
			    if (product.getBuild(previous) != null && ControlCenter.INSTANCE.getProductsMinVersion() != null &&
			            !ControlCenter.INSTANCE.getProductsMinVersion().isGreaterOrEqualTo(product.getBuildVersion(previous))) {
			        File previousWorkspace = new File(this.product.getLocalWorkspace() + File.separator + previous);
	                if (previousWorkspace.isDirectory()) {
	                    build.workspace.mkdirs();
	                    try {
	                        FileUtils.copyDirectory(previousWorkspace, build.workspace, true);
	                    } catch (IOException e) {
	                        // screw it
	                    }
	                }
			    } else {
			        System.err.println("Uncompatible version of product, donwload all: "+ previous);
			    }
				
			}

			Distribution ret = new Distribution(build.getDownloadUrl(), build.workspace);
			ret.setListener(listener);
			ret.sync();
			return ret;
		}

		throw new RuntimeException("Internal error: no build for download");
	}

}
