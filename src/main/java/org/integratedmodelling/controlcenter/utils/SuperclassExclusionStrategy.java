package org.integratedmodelling.controlcenter.utils;

import java.util.HashSet;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class SuperclassExclusionStrategy implements ExclusionStrategy {
    
    private HashSet<String> used = new HashSet<String>();

    public boolean shouldSkipClass(Class< ? > clazz) {
        return false;
    }

    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        String fieldName = fieldAttributes.getName();
        return !used.add(fieldName);
    }

}
