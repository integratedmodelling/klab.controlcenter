package org.integratedmodelling.controlcenter;

public interface Timestamp {

	/*
	 * ----------------------------------------------------------------------------
	 * ACHTUNG ACHTUNG ACHTUNG ACHTUNG etc.
	 * ----------------------------------------------------------------------------
	 * THIS MUST BE UPDATED TO THE CURRENT DATE BEFORE RUNNING 'mvn package' TO
	 * BUILD A NEW CONTROL CENTER. THEN IT MUST BE COMMENTED OUT AND CHANGED AGAIN
	 * TO @@@@ BEFORE COMMITTING.
	 * ----------------------------------------------------------------------------
	 */
//	public static final String BUILD_TIMESTAMP = "2010-10-18T11:00:00";
	public static final String BUILD_TIMESTAMP = "@@@@@";
}
