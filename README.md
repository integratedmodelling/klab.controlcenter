# k.LAB Control Center

The k.LAB Control Center manages the authentication, download, update and execution of all k.LAB products, including the k.LAB engine (with its web-based user interface, k.Explorer) and the modeler's user interface (k.Modeler).

On systems where you can use a Control Center installer, it is not necessary to have Java installed, as the Control Center can download it transparently if needed.

To download, install, and launch the Control Center, follow the instruction [here](https://www.integratedmodelling.org/statics/pages/gettingstarted.html)

The main active developers are:

* Ferdinando Villa (lead developer and designer)
* Enrico Girotto (Web development, release engineering)
* Steven Wohl (authentication, server infrastructure)

Any inquiries should be directed to info@integratedmodelling.org.


